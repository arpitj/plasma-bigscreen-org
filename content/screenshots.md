---
title: Screenshots
layout: page
screenshots:
  - url: /img/pages/screenshots/screenshot-2.png
  - url: /img/pages/screenshots/screenshot-3.png
  - url: /img/pages/screenshots/screenshot-4.png
  - url: /img/pages/screenshots/screenshot-5.png
  - url: /img/pages/screenshots/screenshot-6.png
  - url: /img/pages/screenshots/screenshot-7.png
  - url: /img/pages/screenshots/screenshot-8.png
    name: "Plasma Bigscreen homescreen"
menu:
  main:
    parent: project
    weight: 2
---

{{< screenshots name="screenshots" >}}
